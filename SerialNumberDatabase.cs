﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Globalization;
using System.Data;


<<<<<<< HEAD
namespace SerialNumberDatabaseDLL
{
    public sealed class SerialNumberDatabaseFunctions : IDisposable
=======
namespace SerialNumberFunctionsDLL
{
    public sealed class SerialNumberFunctions : IDisposable
>>>>>>> release/dll_namechange
    {
        public string LastErrorMessage = string.Empty;
        public int LastErrorCode = 0;

        public enum SNErrorCodes
        {
            none,                   // 0  - no errors
            open = -1,                   // 1  - database open error
            locking = -2,                // 2  - error locking the table
            recordingData = -3,          // 3  - error trying to record data
            not_open = -4,               // 4 -  expected database to be open
            last_sn_read = -5,           // 5  - error reading the last serial number
            dup_check = -6,              // 6  - database error looking for duplicate sn
            dup_sn_found = -7,           // 7  - duplicate sn was found
            reading = -8,                // 8  - error reading data
            mac_get_last_used = -9,      // 9  - error reading the last used mac
            mac_get_start_stop = -10,     //`10 - error reading the start/stop mac
            mac_out_of_range = -11,        // 11 - new mac will be out of start/stop range
        }

        private string outmsg = string.Empty;

        private struct databaseInfoData
        {
            public string user;
            public string password;
            public string server;
            public string database;
        }

        private databaseInfoData databaseInfo = new databaseInfoData();
        private string mysqlConnectString = string.Empty;
        private MySqlConnection connection = null;
        private bool disposed = true;

        private string stationName = string.Empty;


<<<<<<< HEAD
        public SerialNumberDatabaseFunctions(string server, string database, string user, string password, string stationName)
=======
        public SerialNumberFunctions (string server, string database, string user, string password, string stationName)
>>>>>>> release/dll_namechange
        {
            this.stationName = stationName;
            databaseInfo.user = user;
            databaseInfo.password = password;
            databaseInfo.server = server;
            databaseInfo.database = database;
            mysqlConnectString = "Server=" + server + ";Database=" + database + ";uid=" + user + ";password=" + password + ";";
            connection = new MySqlConnection(mysqlConnectString);
        }


        public void Dispose()
        {
            LastErrorMessage = string.Empty;
            if (connection != null)
            {
                connection.Close();
                connection.Dispose();
                this.disposed = true;
                GC.SuppressFinalize(this);
            }
        }

        /// <summary>
        /// will check to see if the database can be opened.
        /// </summary>
        /// <returns>true = can open</returns>
        public bool CheckForDatabase()
        {
            LastErrorMessage = string.Empty;
            LastErrorCode = 0;
            try
            {
                connection.Open();
            }
            catch (Exception ex)
            {
                LastErrorMessage = "Error opening " + databaseInfo.database + " on server " + databaseInfo.server  + ". " + ex.Message;
                LastErrorCode = (int)SNErrorCodes.open;
                return false;
            }
            connection.Close();
            return true;
        }



        //---------------------------------------------------------------
        // GetAndRecordNextSN
        /// <summary>
        /// Will get the next availible SN for a given product family and
        /// record it.
        /// </summary>
        /// <remarks>
        /// It will use the current date to figure out the week/year part of the SN.
        ///
        ///      Generate the first part of the SN.
        ///          factoryCode productFamily version
        ///       get the last serial number for that product code from the lastused table
        ///       update the lastused table with the new last used serial number
        ///      If that group does not exist, than use 00001.
        ///      Else add 1 to the highest sn # found.
        ///      
        /// The SerialNumberDatabase will be locked during this process.
        /// Returns "" if there is a problem.
        /// 
        /// LastErrorCodes
        /// 1 - SNErrorCodes.open, error opening database
        /// 2 - SNErrorCodes.locking, error locking table
        /// 3 - SNErrorCodes.recordingData, error recording sn data
        /// 4 - SNErrorCodes.not_open, expected database to be open
        /// 6 - SNErrorCodes.dup_check, error reading database for sn
        /// 7 - SNErrorCodes.dup_sn_found, duplicate sn was found
        /// </remarks>
        /// <param name="factoryCode"></param>
        /// <param name="familyName"></param>
        /// <param name="version"></param>
        /// <returns></returns>
        public string GetAndRecordNextHLASN(string hlaPrefix)
        {
            MySqlDataReader rdr = null;
            MySqlCommand cmd = null;
            string nextSN = string.Empty;
            outmsg = string.Empty;
            LastErrorMessage = string.Empty;
            LastErrorCode = 0;
            string fixedSNpart = string.Empty;
            CultureInfo ciCurr = CultureInfo.CurrentCulture;
            int weekNum = ciCurr.Calendar.GetWeekOfYear(DateTime.Now, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday);

            try
            {
                connection.Open();
            }
            catch (Exception ex)
            {
                LastErrorMessage = "Error opening database. " + ex.Message;
                LastErrorCode = (int)SNErrorCodes.not_open;
                return string.Empty;
            }

            cmd = connection.CreateCommand();

            // lock the lastused table
            try
            {
                cmd.CommandText = string.Format("LOCK TABLE last_hla_sn_used WRITE, last_hla_sn_used AS FAMILY2 READ");
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                connection.Close();
                LastErrorMessage = "Error locking table. " + ex.Message;
                LastErrorCode = (int)SNErrorCodes.locking;
                return string.Empty;
            }


            // see if this product code+version+date exists
            fixedSNpart = string.Format("{0}{1}{2:D2}", hlaPrefix, DateTime.Now.ToString("yy"), weekNum);
            cmd.CommandText = "SELECT sn_prefix, sn, last_creation_date FROM last_hla_sn_used WHERE sn_prefix = '" + fixedSNpart + "';";
            rdr = cmd.ExecuteReader();
            if (rdr.Read())            // if a entry was found
            {
                // found something, so replace it with the updated info
                nextSN = (Convert.ToInt32(rdr.GetString(1)) + 1).ToString("D5");
                rdr.Close();
                cmd.CommandText = "REPLACE INTO last_hla_sn_used(sn_prefix, sn, last_creation_date) " +
                    "VALUES(@SN_PREFIX, @SN, @LAST_CREATION_DATE)";
                cmd.Parameters.AddWithValue("@SN_PREFIX", fixedSNpart);
                cmd.Parameters.AddWithValue("@SN", nextSN);
                cmd.Parameters.AddWithValue("@LAST_CREATION_DATE", DateTime.UtcNow);
            }
            else            // nothing found, so start a new series
            {
                // new entry
                nextSN = "00000";
                rdr.Close();
                cmd.CommandText = "INSERT INTO last_hla_sn_used(sn_prefix, sn, last_creation_date) " +
                    "VALUES(@SN_PREFIX, @SN, @LAST_CREATION_DATE)";
                cmd.Parameters.AddWithValue("@SN_PREFIX", fixedSNpart);
                cmd.Parameters.AddWithValue("@SN", nextSN);
                cmd.Parameters.AddWithValue("@LAST_CREATION_DATE", DateTime.UtcNow);
            }
            cmd.ExecuteNonQuery();          // record into last_sn_used
            cmd.CommandText = "UNLOCK TABLES";
            cmd.ExecuteNonQuery();
            rdr.Close();
            cmd.Dispose();
            connection.Close();

            return fixedSNpart + nextSN;
        }



        /// <summary>
        /// Record the serial number data.
        /// </summary>
        /// <remarks>
        /// 3 - SNErrorCodes.recordingData, error recording sn data
        /// 4 - SNErrorCodes.not_open, expected database to be open
        /// 6 - SNErrorCodes.dup_check, error reading database for sn
        /// 7 - SNErrorCodes.dup_sn_found, duplicate sn was found
        /// </remarks>
        /// <param name="sn"></param>
        /// <param name="pn"></param>
        /// <param name="pcaSN"></param>
        /// <param name="mac"></param>
        /// <param name="cmd"></param>
        /// <returns>
        /// true = ok, recorded
        /// false = dup or error
        /// </returns>
        public bool RecordSNData(string sn, string pcaSN, List<string> mac)
        {
            string assignedSN = string.Empty;
            string lastSN = string.Empty;
            string cmdText;
            MySqlCommand cmd = null;

            try
            {
                connection.Open();
            }
            catch (Exception ex)
            {
                LastErrorMessage = "Error opening database. " + ex.Message;
                LastErrorCode = (int)SNErrorCodes.not_open;
                return false;
            }


            if (IsSNADuplicate(sn))        // verify that it is not a dup. 
            {
                return false;                           // there as a dup or error. IsSNADuplicate will set LastErrorxxxx
            }

            // now know that it is not a dup
            try
            {
                cmd = connection.CreateCommand();
                cmdText = "INSERT INTO all_sns (hla_sn, pcba_sn, creation_date, mac, mac2) " +
                    "VALUES(@SN,  @PCA_SN, @CREATION_DATE, @MAC, @MAC2)";
                cmd.Parameters.AddWithValue("@SN", sn);
                cmd.Parameters.AddWithValue("@PCA_SN", pcaSN);
                cmd.Parameters.AddWithValue("@CREATION_DATE", DateTime.UtcNow);
                if (mac.Count != 0)
                    cmd.Parameters.AddWithValue("@MAC", mac.ElementAt(0));
                else
                    cmd.Parameters.AddWithValue("@MAC", "");
                if (mac.Count == 2)
                    cmd.Parameters.AddWithValue("@MAC2", mac.ElementAt(1));
                else
                    cmd.Parameters.AddWithValue("@MAC2", "");
                cmd.CommandText = cmdText;
                cmd.ExecuteNonQuery();
                connection.Close();
                return true;
            }
            catch (Exception ex)
            {
                LastErrorMessage = "Error inserting SN record. " + ex.Message;
                LastErrorCode = (int)SNErrorCodes.recordingData;
                connection.Close();
                return false;
            }
        }

        //--------------------------------------------
        // IsSNADuplicate
        //
        //  Checks to see if this hla SN exists.
        //
        // Parameters:
        //      string sn - sn to check
        //      string familyName - the family name of the product
        //
        // Return:
        //      true - sn was found
        //      false - no sn found or error
        /// <summary>
        /// Checks to see if this SN exists for a family. if return true, check LastErrorCode/LastErrorMessage for errors.
        /// </summary>
        /// <remarks>
        /// LastErrorCode
        /// 4 - SNErrorCodes.not_open, expected database to be open
        /// 6 - SNErrorCodes.dup_check, error reading database for sn
        /// </remarks>
        /// <param name="sn"></param>
        /// <param name="familyName"></param>
        /// <returns>true = sn found or error</returns>
        private bool IsSNADuplicate(string sn)
        {
            string assignedMac = string.Empty;
            string lastMac = string.Empty;
            MySqlDataReader rdr = null;
            MySqlCommand cmd = null;
            bool status;
            LastErrorMessage = string.Empty;
            LastErrorCode = 0;

            if (connection.State != System.Data.ConnectionState.Open)
            {
                LastErrorMessage = "Serial Number database has not been opened yet.";
                LastErrorCode = (int)SNErrorCodes.not_open;
                return true;
            }

            cmd = connection.CreateCommand();
            string startmac = string.Empty, endmac = string.Empty;

            // verify that it is not a dup. 
            cmd.CommandText = "SELECT hla_sn FROM all_sns WHERE hla_sn= '" + sn + "';";
            try
            {
                rdr = cmd.ExecuteReader();
                if (rdr.Read())        // if found something
                {
                    status = true;
                    LastErrorCode = (int)SNErrorCodes.dup_sn_found;
                    LastErrorMessage = "SN " + sn + " is a duplicate.";
                }
                else
                    status = false;
                rdr.Close();
            }
            catch (Exception ex)
            {
                LastErrorMessage = "SN Dup check error. " + ex.Message;
                LastErrorCode = (int)SNErrorCodes.dup_check;
                status = true;
            }
            return status;
        }


        /// <summary>
        /// Will look to see if the PCBA SN exists in the database
        /// </summary>
        /// <remarks>
        /// Error codes
        /// 1   open error
        /// 8   reading data
        /// </remarks>
        /// <param name="productCode">HLA product code</param>
        /// <param name="pcbaSN"></param>
        /// <param name="hlaSN"></param>
        /// <param name="mac1"></param>
        /// <param name="mac2"></param>
        /// <returns></returns>
        public bool LookForPCBASN(string pcbaSN, ref string hlaSN, ref string mac1, ref string mac2)
        {
            bool status = true;
            MySqlDataReader rdr = null;
            MySqlCommand cmd = null;

            hlaSN = string.Empty;
            mac1 = string.Empty;
            mac2 = string.Empty;

            try
            {
                connection.Open();      // open the database
            }
            catch (Exception ex)
            {
                LastErrorMessage = "Serial Number database open error. " + ex.Message;
                LastErrorCode = (int)SNErrorCodes.open;
                return false;
            }

            cmd = connection.CreateCommand();


            // look for the pcba sN
            cmd.CommandText = "SELECT hla_sn, pcba_sn, mac, mac2 FROM all_sns WHERE pcba_sn= '" + pcbaSN + "';";
            try
            {
                rdr = cmd.ExecuteReader();
                if (rdr.HasRows)
                {
                    while (rdr.Read())        // just in case it has been assing multiple MACs. get the last
                    {
                        hlaSN = rdr.GetString(0);
                        mac1 = rdr.GetString(2);
                        mac2 = rdr.GetString(3);
                        status = true;
                    }
                }
                else
                    status = false;
                rdr.Close();
            }
            catch (Exception ex)
            {
                connection.Close();
                LastErrorMessage = "Look for pcba SN error. " + ex.Message;
                LastErrorCode = (int)SNErrorCodes.reading;
                return false;
            }

            connection.Close();
            return status;
        }

        /// <summary>
        /// Will look to see if the HLA SN exists in the database
        /// </summary>
        /// <remarks>
        /// Error codes
        /// 1   open error
        /// 8   reading data
        /// </remarks>
        /// <param name="productCode">HLA product code</param>
        /// <param name="pcbaSN"></param>
        /// <param name="hlaSN"></param>
        /// <param name="mac1"></param>
        /// <param name="mac2"></param>
        /// <returns></returns>
        public bool LookForHLASN(string hlaSN, ref string pcbaSN, ref string mac1, ref string mac2)
        {
            bool status = true;
            MySqlDataReader rdr = null;
            MySqlCommand cmd = null;

            pcbaSN = string.Empty;
            mac1 = string.Empty;
            mac2 = string.Empty;

            try
            {
                connection.Open();      // open the database
            }
            catch (Exception ex)
            {
                LastErrorMessage = "Serial Number database open error. " + ex.Message;
                LastErrorCode = (int)SNErrorCodes.open;
                return false;
            }

            cmd = connection.CreateCommand();


            // look for the pcba sN
            cmd.CommandText = "SELECT hla_sn, pcba_sn, mac, mac2 FROM all_sns WHERE hla_sn= '" + hlaSN + "';";
            try
            {
                rdr = cmd.ExecuteReader();
                if (rdr.HasRows)
                {
                    while (rdr.Read())        // just in case it has been assing multiple MACs. get the last
                    {
                        pcbaSN = rdr.GetString(1);
                        mac1 = rdr.GetString(2);
                        mac2 = rdr.GetString(3);
                        status = true;
                    }
                }
                else
                    status = false;
                rdr.Close();
            }
            catch (Exception ex)
            {
                connection.Close();
                LastErrorMessage = "Look for hla SN error. " + ex.Message;
                LastErrorCode = (int)SNErrorCodes.reading;
                return false;
            }

            connection.Close();
            return status;
        }



        //--------------------------------------
        //
        //      MAC functions
        //
        //----------------------------------------

        /// <summary>
        /// Will get for the next MAC from the family.
        /// <param name="family"></param>
        /// <param name="pcbaSN"></param>
        /// <param name="stationName"></param>
        /// <param name="MAC"></param>
        /// <param name="failStatus"></param>
        /// <returns>
        /// true - success
        /// false - error. check LastErrorCode and LastErrorMessage
        /// </returns>
        public bool GetAndRecordNextMac(string family, int numOfMacs, ref string mac1, ref string mac2)
        {
            string hlaSN = string.Empty;
            mac1 = string.Empty;
            mac2 = string.Empty;

            string lclmac1 = string.Empty;
            string lclmac2 = string.Empty;
            bool status = true;
            List<string> lclMAC = new List<string>();
            MySqlDataReader rdr = null;
            MySqlCommand cmd = null;
            cmd = connection.CreateCommand();
            LastErrorCode = 0;
            LastErrorMessage = string.Empty;


            lclMAC.Clear();
            try
            {
                connection.Open();
            }
            catch (Exception ex)
            {
                LastErrorMessage = "Error opening database. " + ex.Message;
                LastErrorCode = (int)SNErrorCodes.not_open;
                return false;
            }
            cmd = connection.CreateCommand();
            try
            { 
                cmd.CommandText = string.Format("LOCK TABLE last_mac_used WRITE");
                cmd.ExecuteNonQuery();
                lclMAC = GetNextMac2(family, ref rdr, cmd);
                if (lclMAC.Count == 0)      // GetNextMac2 will set LastErrorxxx if error
                {
                    cmd.CommandText = "UNLOCK TABLES";
                    cmd.ExecuteNonQuery();
                    connection.Close();
                    return false;
                }
                mac1 = lclMAC.ElementAt(0);
                if (lclMAC.Count == 2)
                    mac2 = lclMAC.ElementAt(1);
                cmd.CommandText = "UNLOCK TABLES";
                cmd.ExecuteNonQuery();
                connection.Close();
            }
            catch (Exception ex)       // LastErrorCode and LastErrorMessage set by functions
            {
                cmd.CommandText = "UNLOCK TABLES";
                cmd.ExecuteNonQuery();
                connection.Close();
                LastErrorMessage = "Error locking or getting next mac. " + ex.Message;
                LastErrorCode = -1111;
                return false;
            }

            return status;
        }

        /// and cmd = connection.CreateCommand() done.
        /// </summary>
        /// <remarks>
        /// LastErrorCodes
        /// 11 - MACErrorCodes.Not_Open, Assumed database should be open
        /// 12 - MACErrorCodes.Last_MAC_Read, Error reading the last mac used for a family
        /// 13 - MACErrorCodes.Read_StartStop, error readin the start/stop macs
        /// 14 - MACErrorCodes.MAC_OutOfRange, the mac to be used is out of the family range
        /// </remarks>
        /// <param name="familyName">mac family name to use</param>
        /// <param name="noOfMacks">number of macs to get. valid 1 or 2</param>
        /// <param name="rdr">reader for the mac database</param>
        /// <param name="cmd">command for the mac database</param>
        /// <returns>
        /// List of strings. If list is empty, there was a error. check LastErrorCode and LastErrorMessage
        /// </returns>
        private List<string> GetNextMac2(string familyName, ref MySqlDataReader rdr, MySqlCommand cmd)
        {
            string assignedMac = string.Empty;
            string assignedMac2 = string.Empty;
            string lastMac = string.Empty;
            UInt64 istartmac, iendmac;
            string startmac = string.Empty, endmac = string.Empty;
            UInt64 nextmac = 0;
            List<string> maclist = new List<string>();
            int noOfMacks = 0;

            if (connection == null)                 // if the database has not been opened
            {
                LastErrorMessage = "Serial Number database has not been opened yet.";
                LastErrorCode = (int)SNErrorCodes.not_open;
                return maclist;
            }


            // last mac is in the last_mac_used.last_mac_used field.
            try
            {
                cmd.CommandText = "SELECT last_mac_used, NUMBER_PER FROM last_mac_used WHERE PRODUCT_FAMILY = '" + familyName + "'";
                rdr = cmd.ExecuteReader();
                rdr.Read();
                if (rdr.HasRows)
                {
                    lastMac = rdr.GetString(0);
                    noOfMacks = rdr.GetInt32(1);
                    rdr.Close();
                }
                else
                {
                    LastErrorMessage = "last_mac_used table does not have a entry for " + familyName;
                    LastErrorCode = (int)SNErrorCodes.reading;
                    rdr.Close();
                    return maclist;
                }

            }
            catch (Exception ex)
            {
                LastErrorMessage = "Error reading last mac used. " + ex.Message;
                LastErrorCode = (int)SNErrorCodes.mac_get_last_used;
                return maclist;
            }

            // make new mac
            nextmac = Convert.ToUInt64(lastMac, 16);                    // last mac in database. make it a number
            nextmac++;                                                  // get next aviablble mac
            assignedMac = String.Format("{0:X12}", nextmac).ToUpper();  // make it a string
            if (noOfMacks == 2)                                         // if two macs needed.
            {
                nextmac++;                                              // get next for mac2
                assignedMac2 = String.Format("{0:X12}", nextmac).ToUpper();

            }
            try
            {
                // verify that the next one is within the product range
                cmd.CommandText = "SELECT start, end FROM last_mac_used WHERE product_family = '" + familyName + "'";
                rdr = cmd.ExecuteReader();
                rdr.Read();
                startmac = rdr.GetString(0);
                endmac = rdr.GetString(1);
                rdr.Close();
            }
            catch (Exception ex)
            {
                LastErrorMessage = "Error reading start/stop macs." + ex.Message;
                LastErrorCode = (int)SNErrorCodes.mac_get_start_stop;
                return maclist;
            }

            istartmac = Convert.ToUInt64(startmac, 16);
            iendmac = Convert.ToUInt64(endmac, 16);
            if ((nextmac > iendmac) || (nextmac < istartmac))
            {
                string msg = string.Format("The new mac is out of range. Start: {0} End: {1} Next:{2}", startmac, endmac, assignedMac);
                LastErrorMessage = msg;
                LastErrorCode = (int)SNErrorCodes.mac_out_of_range;
                return maclist;
            }
            // update the last mac used
            maclist.Add(assignedMac);
            if (noOfMacks == 2)
                maclist.Add(assignedMac2);
            cmd.CommandText = "UPDATE last_mac_used SET last_mac_used =  '" + maclist.Last() + "' WHERE PRODUCT_FAMILY = '" + familyName + "'";
            cmd.ExecuteNonQuery();
            return maclist;
        }


    }
}
